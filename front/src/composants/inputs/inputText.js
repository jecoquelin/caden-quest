import './../../style/css/inputText.css';

const InputText = ({ type, id, value, placeholder, onChange }) => {
    return (
        <input
            type={type ? type : "text"}
            id={id} 
            value={value} 
            placeholder={placeholder} 
            onChange={onChange} 
        />
    );
};

export default InputText;