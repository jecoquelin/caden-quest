import BoutonDecision from './boutonDecision';

const ActionSection = ({ typeAction, destinations }) => {

    console.log("DATA  : " + JSON.stringify(destinations))

    return (
        <>
            {typeAction === "choix simple" && (
                // Pour chaque élément présent dans destinations, on ajoute un bouton sur la même ligne
                <>
                    {destinations.map((destination, index) => (
                        <BoutonDecision key={index} texte={destination} />
                    ))}
                </>
            )}
            {typeAction === "enigme" && (
                <input type="text" placeholder='Réponse'/>
            )}
        </>
    );
};

export default ActionSection ;