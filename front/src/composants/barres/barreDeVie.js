import React from 'react';

const BarreDeVie = ({ vieDebut, vieRestante }) => {

    let remplissageBarre = (vieRestante / vieDebut) * 100;
    let couleurBarre = '#FFF';

    console.log("VIE : " + vieRestante + " / " + vieDebut);
    
    if (remplissageBarre < 33) {
        couleurBarre = '#FF0000'; // rouge
    } else if (remplissageBarre < 66) {
        couleurBarre = '#FFA500'; // jaune / orange
    } else {
        couleurBarre = '#00FF00'; // vert
    }

    const containerStyles = {
        position: 'relative',
        height: 20,
        width: '100%',
        backgroundColor: "#e0e0de",
        borderRadius: 50,
        overflow: 'hidden'
    };
    
    const fillerStyles = {
        height: '100%',
        width: `${remplissageBarre}%`,
        margin: '0 auto 0 0',
        backgroundColor: couleurBarre,
        borderRadius: 'inherit',
        transition: 'width 0.5s ease-in-out'
    };
    
    const labelContainerStyles = {
        position: 'absolute',
        height: '100%',
        width: '100%',
        zIndex: 1
    };
    
    const labelStyles = {
        padding: 5,
        color: 'black',
        fontWeight: 'bold'
    };

    return (
        <div style={containerStyles}>
            <div style={fillerStyles}></div>
            <div style={labelContainerStyles}>
                <span style={labelStyles}>{`${vieRestante} / ${vieDebut}`}</span>
            </div>
        </div>
    );
}

export default BarreDeVie;
