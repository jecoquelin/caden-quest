import H2 from '../titres/h2';
import './../../style/css/popup.css';
import './../../style/css/popupRecommencer.css';

function PopupRecommencer({ fermerPopup, children }) {
    return (
        <div className='popup popup-recommencer'>
            <div className='popup-conteneur'>
                <div className='conteneur-interne'>
                    <span className='fermer' onClick={fermerPopup}>×</span>
                    <div className='popup-titre'>
                        <H2 titre="Recommencer ?"></H2>
                        <p>Cette action suprimera votre partie actuelle, êtes-vous sûr de vouloir continuer ?</p>
                    </div>
                    <div className='popup-contenu'>
                        {children}
                    </div>
                </div>
            </div>
        </div>
    );
}

export default PopupRecommencer;