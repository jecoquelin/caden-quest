import './../../style/css/popupInfo.css';
import { forwardRef, useImperativeHandle } from 'react';

const PopupInfo = forwardRef(( {texte, couleur}, ref) => {
    const publicRef = {
        afficherPopup: (texte, couleur, type, seconde=3) => {
            console.log("type", type)
            // Supprimer toute les classes du toast pour éviter qu'elles se chevauchent
            document.getElementById('popupInfo').classList = '';
            document.getElementById('popupInfo').classList.add('popupInfo');


            document.getElementById('popupInfo').classList.add('show');
            document.getElementById('popupInfo').classList.add(type);
            document.getElementById('popupInfo').innerText = texte
            document.getElementById('popupInfo').style.backgroundColor = couleur
            console.log("sldkfhskdf")
            setTimeout(() => {
                document.getElementById('popupInfo').classList.remove('show');
            }, seconde*1000);
        }
    };
    useImperativeHandle(ref, () => publicRef);

    return (
        <div id="popupInfo" className="popupInfo" style={{backgroundColor: couleur}}>
            {texte}
        </div>
    )
});

export default PopupInfo