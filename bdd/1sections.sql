SET SCHEMA 'cayden_quest';

DO $$
BEGIN
    IF EXISTS (SELECT 1 FROM information_schema.tables WHERE table_name = '_section') THEN
        -- EXECUTE 'TRUNCATE TABLE _joueur' ;
        -- EXECUTE 'TRUNCATE TABLE _ennemi';
        -- EXECUTE 'TRUNCATE TABLE _personnage CASCADE'; -- Attention suppression des _joueurs et des _ennemis et  des _personnage
        EXECUTE 'TRUNCATE TABLE _section CASCADE'; -- Suppression des _ennemis mais pas des _personnage et des _section
        EXECUTE 'TRUNCATE TABLE _item CASCADE' ; -- suppresion les _item et les _inventaire
    END IF;
END $$;



INSERT INTO _section (id, texte, type_action, destinations) VALUES (
    1,
    'La première chose qui vous frappe, alors que vous roulez à vive allure sur la route qui s''ouvre devant vous, est la rapidité à laquelle tout s''est dégradé. Vous n''aviez jamais réalisé, jusqu''à présent, combien l''entretien régulier du matériel, des routes et des constructions était indispensable au maintien de la civilisation. Tout autour de vous, des bâtiments tombent en ruine, les carcasses rouillées de voitures abandonnées encombrent la voie dans un désordre indescriptible, l''herbe et le chiendent, désormais livrés à eux-mêmes, ont envahi la chaussée, et des hordes de chiens sauvages rôdent en toute liberté. A quinze kilomètres environ de Nouvelle Espérance, vous faites halte dans une petite ville et vous coupez le contact. Seul, le hurlement lugubre d''un chien solitaire vient rompre le silence de mort qui plane sur l''agglomération. Vous êtes tenté de descendre de voiture pour explorer les lieux, mais vous savez que ce serait prendre là un risque inutile, aussi préférez vous quitter cet endroit sans plus tarder. Mais au moment où vous vous apprêtez à redémarrer, une détonation déchire le silence.',
    'choix simple',
    '{"Sortir de l''interceptor":2, "Quitter la ville maitenant":3}'
);

INSERT INTO _section (id, texte, type_action, destinations) VALUES (
    2,
    'Vous traversez la rue en courant et vous vous aplatissez contre le mur d''un immeuble, vous attendant, à tout instant, à entendre claquer une autre détonation. Le cœur battant la chamade, vous rampez prudemment jusqu''à l''angle du bâtiment et vous observez les alentours. Lancez 2 dés par rapport à votre niveau de PERCEPTION',
    'lancé de dés',
    '{"<=Perception":4, ">Perception":5}'
);

INSERT INTO _section (id, texte, type_action, destinations) VALUES (
    3,
    'Vous sortez rapidement de la ville, zigzaguant entre les épaves de voitures et les arbres abattus qui encombrent la voie. Un peu plus loin, vous voyez que la route rejoint l''autoroute du sud. Il y a une petite station-service à la jonction, portant comme enseigne « le Garage du Sud ». Vous vous y arrêtez, car vous êtes fort intrigué par un hot-rod d''un rouge éclatant, garé le long du bâtiment. Le bolide a l''air bien entretenu et en état de marche. Soudain, une jeune fille, vêtue d''un blue-jean et d''un T-shirt, sort du bureau et vous demande en souriant : « Salut ! Est-ce que je peux vous être utile ? ».',
    'choix simple',
    '{"Sortir de la voiture":6, "Quitter cet endroit":7}'
);

INSERT INTO _section (id, texte, type_action, destinations) VALUES (
    4,
    'Vous apercevez une personne qui se cache à l’encadrement d’une porte et qui semble attendre une personne avec un fusil à la main. En comprenant que vous êtes en position de faiblesse, vous préférez rebrousser chemin. Alors que vous vous apprêter à reprendre votre voiture, une personne vous plaque au sol. Vous réussissez à vous débattre de son emprise. Vous vous trouvez en face de lui. Il ne ressemble pas à celui que vous avez vu quelques minutes auparavant.',
    'choix simple',
    '{"Fuir":8, "Attaquer":9}'
);

INSERT INTO _section (id, texte, type_action, destinations) VALUES (
    5,
    'Il ne semble y avoir personne en vue dans une étroite ruelle. Alors que vous avancez à découvert, vous entendez soudain une voix crier : « Ne faites pas un pas de plus, ou vous êtes un homme mort  ! D''où venez-vous ? »',
    'choix simple',
    '{"Avouer nos origines":10, "Prétendre être un guerrier de la route":11}'
);

INSERT INTO _section (id, texte, type_action, destinations) VALUES (
    6,
    'Vous ouvrez la portière et descendez de l''Interceptor. Alors que vous avancez vers la fille, l''homme au crâne rasé surgit de derrière une pompe, tenant à la main une arbalète. Il vous crie : « Quelle surprise ! Allez, vite ! les clés et les crédits, et n''essaye pas de jouer au plus malin avec moi si tu ne veux pas être blessé ! »',
    'choix simple',
    '{"Obéir":12, "Se battre":13}'
);

INSERT INTO _section (id, texte, type_action, destinations) VALUES (
    7,
    'Vous roulez à vive allure sur une route relativement dégagée, savourant la liberté de rouler sans contrainte. Cependant, votre euphorie est brutalement interrompue par l''arrivée d''une Chevrolet rouge, arme braquée depuis une tourelle aménagée sur son toit. Sans hésitation, vous enfoncez l''accélérateur, déclenchant une fuite désespérée. Les balles sifflent autour de vous, percutant votre véhicule et éclatant le verre. Manœuvrant habilement à travers les débris, vous parvenez à distancer momentanément votre poursuivant, gagnant du terrain grâce à votre vitesse et votre agilité. Au loin, une bifurcation apparaît, vous forçant à prendre une décision cruciale : continuer sur votre trajectoire actuelle ou tenter d''échapper à la Chevrolet en empruntant une route secondaire.',
    'choix simple',
    '{"Poursuivre votre route actuelle":14, "Empreinter la route secondaire":15}'
);

INSERT INTO _section (id, texte, type_action, destinations) VALUES (
    8,
    'Vous glissez votre main dans votre poche pour prendre les clés de la voiture. Vous n’avez que quelques mètres à faire pour rentrer dans la voiture et partir. Cependant, vous avez eu le souffle coupé quand vous avez été plaqué au sol. Lancez 2 dés par rapport à votre niveau d’ENDURANCE',
    'lancé de dés',
    '{"<ENDURANCE":17, ">=ENDURANCE":16}'
);

-- Fin de partie négative
INSERT INTO _section (id, texte, type_action, destinations) VALUES (
    9,
    'Vous courez vers votre adversaire bien décidé à ne pas vous laisser faire. Vous essayez de lui mettre un coup de poing au niveau de la tête mais celui-ci esquive et vous plante une dague, qu’il cachait dans sa poche, en plein coeur. Vous sentez immédiatement la douleur que ce coup vous procure. Vous tombez au sol et vous vous évanouissez sans jamais vous réveiller.',
    'fin de partie',
    'negative'
);

INSERT INTO _section (id, texte, type_action, destinations) VALUES (
    10,
    'Soudain, un homme apparaît dans l''encadrement de la porte et marche vers vous, le fusil pointé dans votre direction. Il vous regarde et vous dit : « Moi aussi, je vais par là. Cela fait trois jours que je circule à moto dans la région depuis que mon break est tombé dans une embuscade où ma femme et mon fils ont trouvé la mort. Je m''étais arrêté ici pour me procurer quelques boîtes de conserve au supermarché d''à côté, quand des chiens furieux m''ont attaqué. J''ai réussi à en tuer un mais les autres se sont enfuis. Je m''appelle Johnson, heureux de vous rencontrer ! ». Il repose son fusil et vous tend une boîte de conserve. *1* Il vous dit qu''il est entrepreneur de son métier puis il vous demande s''il est encore loin de Nouvelle Espérance et s''il a une chance d''y être accueilli. Vous lui répondez que Nouvelle Espérance n''est qu''à une quinzaine de kilomètres et qu''il y sera sûrement le bienvenu car la ville a besoin de gens qualifiés. Vous lui parlez également de votre mission et il vous dit de ne pas vous arrêter au « garage du sud » qui se trouve à environ huit kilomètres de la ville. « Ils n''ont pas une goutte d''essence et ils volent les gens qui s''arrêtent pour faire le plein. » Vous remerciez Johnson pour son conseil et vous lui souhaitez bonne chance. Vous retournez à l''Interceptor. Vous tournez la clé de contact, aussitôt le moteur rugit, et vous démarrez en faisant crisser vos pneus',
    'choix simple',
    '{"skrrrt !":3}'
);

INSERT INTO _item(id, nom, type_effet, niveau_effet) VALUES (
    1,
    'conserve',
    'niveau_endurance',
    2
);

-- Gestion de la perte de point (-1 chance) TODO balise pour perte de point
INSERT INTO _section (id, texte, type_action, destinations) VALUES (
    11,
    'L''homme surgit soudain dans l''encadrement d''une porte et avance en braquant son fusil sur vous. Il vous dévisage avec dureté et dit : « Vous m''avez tout l''air d''être l''homme qui a provoqué la mort de ma femme et de mon fils en faisant exploser mon break, la semaine dernière. Je vais enfin pouvoir les venger ! Mais je me battrai selon les règles. Dégainez lorsque vous serez prêt.» Vous réalisez l''erreur que vous avez commise en mentant à cet homme et vous perdez 1 point de CHANCE |niveau_chance:-1| .',
    'choix simple',
    '{"Convaincre de mon innoncence":18, "Dégainer mon arme":19}'
);

-- Fin de partie négative
INSERT INTO _section (id, texte, type_action, destinations) VALUES (
    12,
    'Au moment où vous tendez vos clés au gangster, celui-ci ricane doucement et vous frappe violemment avec son arbalète. Le coup vous fait perdre connaissance et quand vous vous réveillez, vous vous apercevez que le couple et leur hot-rod ont disparu mais... votre Interceptor aussi !',
    'fin de partie',
    'negative'
);

INSERT INTO _section (id, texte, type_action, destinations) VALUES (
    13,
    'Vous agrippez fermement le manche de votre poignard et vous vous ramassez sur vous-même en attendant que le gangster fasse le premier mouvement. Celui-ci pousse soudain un hurlement et se jette sur vous.',
    'combat',
    '{"victoire":20, "defaite":21}'
);

-- Ajout d'un personnage et/ou ennemi si 'gangster' lorqu'il n'existe pas
-- Ennemi "Ganster" habilete:7 endurance:10
INSERT INTO _personnage(nom, niveau_habilete, niveau_endurance)
SELECT 'Gangster', 7, 10
WHERE NOT EXISTS (
    SELECT 1
    FROM _personnage
    WHERE nom = 'Gangster'
);

INSERT INTO _ennemi(id_personnage, id_section)
SELECT p.id, 13
FROM _personnage p
WHERE p.nom = 'Gangster'
AND NOT EXISTS (
    SELECT 1
    FROM _ennemi e
    WHERE e.id_section = 13
);


INSERT INTO _section (id, texte, type_action, destinations) VALUES (
    14,
    'Vous maintenez le cap, fonçant droit devant vous à travers le paysage dévasté. Malgré la tension qui serre votre poitrine, vous refusez de fléchir, déterminé à surmonter les obstacles qui se dressent sur votre chemin. La Chevrolet rouge tente désespérément de vous rattraper, mais vous parvenez à maintenir une légère avance, exploitant chaque avantage que vous pouvez trouver. Finalement, après une course effrénée qui semble durer une éternité, vous parvenez à distancer votre poursuivant. Sur votre route vous voyez une station service abandonnée. Après votre course éfrenné vous avez consommé beaucoup d’essence.',
    'choix simple',
    '{"Explorer la station service":22, "Continuer sa route":23}'
);

-- Fin de partie négative
INSERT INTO _section (id, texte, type_action, destinations) VALUES (
    15,
    'Vous tournez brusquement sur la route secondaire, espérant échapper à votre poursuivant. La Chevrolet rouge semble hésiter un instant, puis elle se lance à votre poursuite, rugissant sur le gravier alors qu''elle vous pourchasse à travers les virages serrés et les passages étroits. Malgré vos efforts pour semer votre poursuivant, la Chevrolet rouge reste toujours à vos trousses, vous forçant à maintenir un rythme effréné. Alors que vous vous engagez dans un virage particulièrement serré, votre pneu avant gauche éclate soudainement, envoyant votre voiture en tête-à-queue sur la route. Vous sentez votre cœur battre la chamade alors que votre véhicule glisse hors de contrôle, faisant des tonneaux sur le gravier. Vous êtes secoué dans tous les sens, désorienté par le bruit assourdissant du métal contre le sol. Tout devient flou alors que vous êtes projeté violemment contre le volant. Quand tout s''arrête enfin, vous vous retrouvez pris au piège dans les débris de votre propre véhicule, le souffle court et la vision troublée. Vous essayez de vous extraire de l''épave, mais votre corps vous refuse obéissance. Vous sentez une douleur lancinante vous envahir alors que vous perdez conscience. Votre histoire se termine ici, alors que vous gisez seul sur cette route désolée, votre destin scellé dans les ténèbres de l''apocalypse.',
    'fin de partie',
    'negative'
);

-- Fin de partie négative
INSERT INTO _section (id, texte, type_action, destinations) VALUES (
    16,
    'Vous courez d’un coup en direction de la voiture mais l’adversaire à réagi plus vite que vous ne le pensiez. Ayant le souffle coupé, il vous rattrape sans difficulté et sors de sa poche une dague. Vous ressentez une vive douleur dans le dos et vous comprenez ce qu’il vient de se passer lorsque vous voyez du sang sur le sol. L’adversaire vous fouille et prend vos clés ainsi que votre pistolet. Il vous abandonne en partant avec la voiture. Vous commencez à avoir froid. Vous vous évanouissez et vous ne vous réveillez plus jamais.',
    'fin de partie',
    'negative'
);

-- Fin de partie positive
INSERT INTO _section (id, texte, type_action, destinations) VALUES (
    17,
    'Vous courez a toute vitesse vers la voiture. L’adversaire, ayant été pris de court, reste sur place 2 secondes avant qu’il comprenne ce que vous faites. Ces 2 secondes ont été precieuses pour vous qui réussissez à prendre votre voiture et à démarrer. Vous partez tellement vite que vos roues ont dérapé sur le sol. Vous décidez de ne plus vous arrêter avant d’arriver à votre destination. En suivant cette idée, vous arrivez à remplir votre mission en allant à San Angelo.',
    'fin de partie',
    'positive'
);

INSERT INTO _section (id, texte, type_action, destinations) VALUES (
    18,
    'De votre ton le plus convaincant, vous lui assurez que vous n''êtes pour rien dans la mort de sa femme et de son fils. Lancez 2 dés par rapport à votre niveau de CHANCE.',
    'lancé de dés',
    '{"<CHANCE":24, ">=CHANCE":25}'
);

INSERT INTO _section (id, texte, type_action, destinations) VALUES (
    19,
    'Vous essayez de dégainer votre arme. Lancez 2 dés par rapport à votre niveau d’HABILETÉ',
    'lancé de dés',
    '{"<=HABILETE":26, ">HABILETE":27}'
);

-- Fin de partie négative
INSERT INTO _section (id, texte, type_action, destinations) VALUES (
    20,
    'Vous faites un bond en arrière pour vous écarter du gangster qui s''écroule lourdement au sol. Mais alors que vous reculez, votre pied se prend dans un morceau de ferraille rouillée, vous faisant trébucher violemment. Vous tombez lourdement sur le bitume, sentant une douleur aiguë irradier depuis votre cheville. Pendant que vous luttez pour vous relever, le gangster, malgré ses blessures, réussit à se remettre sur pied. D''un geste rapide, il se saisit de son arbalète abandonnée et, dans un ultime effort, tire une flèche en votre direction. Vous n''avez pas le temps de réagir. La pointe acérée de la flèche vous transperce de part en part, mettant fin à vos espoirs de survie. Votre vision se trouble alors que vous sentez vos forces vous quitter, tandis que les ténèbres de la mort vous enveloppent lentement mais inexorablement. Votre aventure s''achève ici, sur cette route désolée, votre nom effacé par les ravages de l''apocalypse',
    'fin de partie',
    'negative'
);

-- Fin de partie négative mais pas trop
INSERT INTO _section (id, texte, type_action, destinations) VALUES (
    21,
    'Vous reprenez conscience quelque temps après, avec un mal de tête épouvantable ! Puis tout vous revient en mémoire et votre première pensée est pour l''Interceptor : qu''en est-il advenu ? Vous vous redressez et regardez tout autour de vous, mais la voiture a disparu... Votre mission se termine ici',
    'fin de partie',
    'negative'
);

INSERT INTO _section (id, texte, type_action, destinations) VALUES (
    22,
    'Avant de descendre, vous examinez la station-service depuis votre voiture. Vous ne voyez aucun mouvement. Vous savez cependant que c’est difficile de voir du mouvement pendant la nuit. Par précaution, vous prenez votre arme et vous descendez. Vous constatez que les pompes sont en bon état. Vous entrez maintenant dans le bâtiment de la station-essence. Vous voyez que ce lieu est délabré. Il n’y a aucune lumière mais dans cette obscurité, des objets pourraient s’avérer utiles ou dangereux. Lancez 2 dés par rapport à votre niveau de PERCEPTION',
    'lancé de dés',
    '{"<=PERCEPTION":28, ">PERCEPTION":23}'
);

-- Fin de partie négative
INSERT INTO _section (id, texte, type_action, destinations) VALUES (
    23,
    'Vous n’avez rien trouvé dans l’obscurité. Vous vous redirigez vers votre voiture. Vous essayez de prendre de l’essence aux pompes et comme vous vous en doutiez, aucune goutte ne sort du pistolet. Tout a été pillé. Vous décidez de rejoindre San Angelo. Arrivé au milieu d’une forêt, la voiture ralentit doucement avant de s’arrêter. Vous voyez que vous n’avez plus d’essence. Vous êtes seul, sans essence, sans espoir. Vous comprenez que c’est la fin. Vous sortez de la voiture et vous errez seul pendant 3 jours avant de tomber mort de fatigue et de soif.',
    'fin de partie',
    'negative'
);

-- Fin de partie positive
INSERT INTO _section (id, texte, type_action, destinations) VALUES (
    24,
    'L''homme vous dévisage durement pendant un long moment avant de vous adresser la parole : « C''est bon, je vous crois mais je voudrais en savoir un peu plus. » Vous lui expliquez alors que ce n''est pas vous qui avez tué sa femme et son fils et que si vous avez menti en prétendant être un guerrier de grand chemin, c''était pour la bonne cause, et uniquement dans le but de ne pas révéler où se trouve Nouvelle Espérance, au cas où l''homme aurait voulu lancer une attaque contre la ville. L''homme a l''air soudain très excité et vous dit : « San Angelo ! Mais c''est là que je vais. Je roule dans sa direction depuis l''embuscade. Je me suis juste arrêté ici pour acheter des boîtes de conserve au supermarché à côté, quand des chiens fous furieux m''ont attaqué. J''en ai abattu un mais les autres se sont enfuis. Je m''appelle Johnson, je suis vraiment désolé de vous avoir traité de cette façon, mais on ne peut vraiment plus faire confiance à personne par les temps qui courent. » Vous lui souriez puis vous lui serrez la main. L''homme reprend : « Je suis entrepreneur en bâtiment. A combien de kilomètres sommes-nous de Nouvelle Espérance ? Et pensez-vous que j''ai une chance de pouvoir y entrer ? » Vous lui répondez que Nouvelle Espérance n''est qu''à une quinzaine de kilomètres et qu''il a de grandes chances d''être accepté car la ville a besoin de gens qualifiés. Vous poursuivez en lui racontant votre mission et il vous prévient de ne pas vous arrêter au Garage du sud qui se trouve à environ huit kilomètres hors de la ville. « Ils n''ont pas une goutte d''essence et ils volent les automobilistes qui s''y arrêtent. » Vous remerciez Johnson pour son précieux conseil et vous lui souhaitez bonne chance. Vous regagnez l''Interceptor et le puissant moteur rugit lorsque vous tournez la clé de contact. La quinzaine de kilomètres se passa sans accro. Vous arrivez à San Angelo en vie !',
    'fin de partie',
    'negative'
);

INSERT INTO _section (id, texte, type_action, destinations) VALUES (
    25,
    'L''homme vous regarde puis il crache par terre avant de vous dire : « Dégaine, je te dis, misérable assassin ! » Vous n''avez pas d''autre choix que de lui obéir',
    'choix simple',
    '{"Obéir":29}'
);

-- Fin de partie positive
INSERT INTO _section (id, texte, type_action, destinations) VALUES (
    26,
    'Vif comme l''éclair, vous dégainez votre revolver et vous le pointez sur l''homme avant que celui-ci ne puisse esquisser le moindre geste. Vous lui ordonnez de lâcher son fusil, puis vous lui expliquez que vous n''êtes pour rien dans l''attentat qui a causé la mort de sa femme et de son fils. Vous lui avouez avoir menti en prétendant être un guerrier de la route afin de protéger l''emplacement de Nouvelle Espérance. L''homme, nommé Johnson, semble surpris par vos paroles mais aussi soulagé. Il vous confie qu''il se dirige justement vers cette ville depuis l''attentat dont il vous a parlé. Vous découvrez qu''il est un entrepreneur en bâtiment, et vous lui assurez que Nouvelle Espérance n''est qu''à une quinzaine de kilomètres. Vous échangez des informations et des conseils, renforçant ainsi votre lien naissant. Après avoir remercié Johnson pour ses conseils précieux, vous lui souhaitez bonne chance dans sa quête et retournez à l''Interceptor. Le moteur rugit de satisfaction lorsque vous tournez la clé de contact, et vous reprenez la route, empli d''un sentiment de soulagement et de satisfaction.Votre voyage vers San Angelo se poursuit, et cette rencontre inattendue vous a donné l''espoir que, malgré les défis et les dangers, un avenir meilleur vous attend dans cette ville.',
    'fin de partie',
    'negative'
);

-- Fin de partie négative
INSERT INTO _section (id, texte, type_action, destinations) VALUES (
    27,
    'Votre revolver est à moitié sorti de son étui lorsque l''étranger presse la gâchette de son fusil. Vous voyez s''élever un mince filet de fumée et vous ressentez, au même instant, une douleur fulgurante à la cuisse droite. Vous êtes projeté en arrière contre le mur et l''écho de la détonation résonne encore dans votre tête lorsque vous vous affalez sur le sol. L''homme tourne les talons et vous abandonne à la férocité des chiens affamés qui rôdent dans les parages. Vous essayez de ramper vers votre voiture cependant, la douleur est tel qu’elle vous empêche d’avancer. Des chiens affamés arrivent vite. Vous leur servez de nourriture.',
    'fin de partie',
    'negative'
);

INSERT INTO _section (id, texte, type_action, destinations, reponse_enigme) VALUES (
    28,
    'Malgré cette obscurité, vous trouvez dans le noir ce qui ressemblerait à une clé parmi les débris ainsi qu’un pied-de-biche. Vous décidez de les prendre. Au loin, sur un mur, vous voyez ceci : "Dans les entrailles de la terre, où règne l''obscurité, Je suis un trésor noir, espoir de mobilité. Trouvez-moi dans cette station, refuge de métal et de béton, Et votre voyage reprendra, loin des affres du temps." Où suis-je ?',
    'enigme',
    '{"trouve":30, "pas trouve":23}',
    'cuves'
);

-- Fin de partie négative
INSERT INTO _section (id, texte, type_action, destinations) VALUES (
    29,
    'Dans un geste nerveux, vous dégainez votre arme, prêt à affronter l''homme qui vous accuse à tort.  Les secondes s''étirent dans une tension palpable alors que vous vous préparez au combat qui semble inévitable. L''homme, le regard empli de rage et de douleur, serre davantage la crosse de son fusil, prêt à faire feu à tout moment. Dans un élan de désespoir, il presse la détente, déclenchant un tir mortel. La balle vous atteint de plein fouet, vous transperçant de part en part, mettant brutalement fin à votre vie. Vous tombez lourdement sur le sol, sentant votre force vous quitter alors que le monde s''obscurcit autour de vous. L''homme, réalisant trop tard son erreur, tombe à genoux à vos côtés, le regard rempli de chagrin et de remords. Mais il est trop tard pour les regrets. Votre histoire s''achève ici, sur cette route désolée, emportant avec elle les espoirs et les promesses d''un avenir meilleur.',
    'fin de partie',
    'negative'
);

-- Fin de partie positive
INSERT INTO _section (id, texte, type_action, destinations) VALUES (
    30,
    'Vous trouvez la réponse et vos yeux tombent sur une mystérieuse trape. Vous descendez et vous arrivez devant porte fermée. Vous utilisez la clé qui, contre toute attente, arrive à ouvrir cette porte. Et, oh merveille ! devant vos yeux ébahis se trouve une cuve à essence. Vous branchez son alimentation et vous sortez de ce sous sol pour rejoindre votre Interceptor. Vous pouvez enfin faire le plein de votre voiture ! Vous vous dirigez ensuite vers Nouvelle Espérance. Après plusieurs heures, vous arrivez enfin à San Angelo, l''objectif de votre mission !',
    'fin de partie',
    'positive'
);