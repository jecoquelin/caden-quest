DROP SCHEMA IF EXISTS cayden_quest CASCADE;
CREATE SCHEMA cayden_quest;
SET SCHEMA 'cayden_quest';

CREATE TABLE _compte(
    id              SERIAL      NOT NULL,
    email           VARCHAR     NOT NULL,
    mot_de_passe    VARCHAR     NOT NULL,
    CONSTRAINT PK_compte PRIMARY KEY (id)
);

CREATE TABLE _personnage (
    id                  SERIAL  NOT NULL,
    nom                 VARCHAR,
    niveau_habilete     INTEGER,
    niveau_endurance    INTEGER,
    CONSTRAINT PK_personnage PRIMARY KEY (id)
);

CREATE TABLE _section (
    id              INT     NOT NULL,
    texte           VARCHAR NOT NULL,
    type_action     VARCHAR, -- Choix simple, lancé de dés, combat, enigme
    destinations    VARCHAR,
    reponse_enigme  VARCHAR,
    CONSTRAINT PK_section PRIMARY KEY (id)
);

CREATE TABLE _joueur (
    id_compte           INTEGER NOT NULL,
    id_personnage       INTEGER NOT NULL,
    id_section_actuelle INTEGER NOT NULL,
    niveau_chance       INTEGER,
    niveau_perception   INTEGER,
    CONSTRAINT PK_joueur PRIMARY KEY (id_compte),
    CONSTRAINT FK_joueur_compte     FOREIGN KEY (id_compte)             REFERENCES _compte(id),
    CONSTRAINT FK_joueur_personnage FOREIGN KEY (id_personnage)         REFERENCES _personnage(id),
    CONSTRAINT FK_joueur_section    FOREIGN KEY (id_section_actuelle)   REFERENCES _section(id)
);

CREATE TABLE _ennemi (
    id_personnage       INTEGER NOT NULL,
    id_section          INTEGER NOT NULL,
    CONSTRAINT PK_ennemi PRIMARY KEY (id_personnage),
    CONSTRAINT FK_ennemi_personnage FOREIGN KEY (id_personnage) REFERENCES _personnage(id),
    CONSTRAINT FK_ennemi_section    FOREIGN KEY (id_section)    REFERENCES _section(id)
);

-- CREATE TABLE _ennemi_actif(
--     id_ennemi               INTEGER NOT NULL,
--     niveau_habilete_actif   INTEGER,
--     niveau_endurance_actif  INTEGER,
--     CONSTRAINT PK_ennemi_actif PRIMARY KEY (id_ennemi),
--     CONSTRAINT FK_ennemi_actif_ennemi FOREIGN KEY (id_ennemi) REFERENCES _ennemi(id_personnage)
-- );



CREATE TABLE _item (
    id              SERIAL NOT NULL,
    nom             VARCHAR,
    type_effet      VARCHAR,
    niveau_effet    INTEGER,
    CONSTRAINT PK_item PRIMARY KEY (id)
);

CREATE TABLE _inventaire (
    id          SERIAL,
    id_compte   INT,
    id_item     INT,
    CONSTRAINT PK_inventaire PRIMARY KEY (id),
    CONSTRAINT FK_inventaire_compte FOREIGN KEY (id_compte) REFERENCES _joueur(id_compte),
    CONSTRAINT FK_inventaire_item   FOREIGN KEY (id_item)   REFERENCES _item(id)
);