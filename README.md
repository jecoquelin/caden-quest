# Cayden Quest

Un jeu dont vous êtes le héros.

## Contexte
Lors du BUT informatique, nous avons eu des projets à réaliser. Celui-ci est l'un deux.

## Description
Nous avons réalisé un projet de livre-jeu en 2 semaines. Nous avons donc repris un livre et nous l'avons adapté à une application web. Ainsi n'importe quelle personne qui se connecte peut jouer. Ce jeu est en solo et il n'y a pas de niveau de difficulté.

## Apprentissage critique

AC31.01 | Choisir et implémenter les architectures adaptées <br>
AC31.02 | Faire évoluer une application existante <br>
AC31.03 | Intégrer des solutions dans un environnement de production <br>
AC36.03 | Guider la conduite du changement informatique au sein d’une organisation


# Structure du projet
## app
Serveur API permettant d'effectuer des actions sur la BDD.

## front
Interface WEB en React s'interfaçant avec l'API.

## bdd
Scripts SQL permetant de mettre en place la BDD et de la peupler avec les sections du livre.

# Mise en place du projet
- `git clone`
- Créer un fichier `.env` dans `app/` avec :
    ```
    DB_USERNAME='' # user postgres
    DB_PASSWORD='' # mdp postgres
    DB_CONTAINER='bdd' # ip ou nom de l'hote bdd
    DB_PORT='5432'
    DB_NAME='cayden'
    TOKEN='' # chaine servant à générer les tokens d'auth
    ```
- Mettre à jour la constante `host` dans `front/index.js` en fonction de l'ip hébergeant l'API
- Créer le dossier `bdd/secrets/` et y ajouter deux fichiers : `postgres-password` et `postgres-user`. Ensuite ajouter seulement le mot de passe et le nom d'utilisateur de la BDD dans leurs fichiers respectifs.
- `docker compose up -d`