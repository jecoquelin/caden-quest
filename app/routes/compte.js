import express from "express";

const routerCompte = express.Router()

// malgré les aparences ce script est très utile puisqu'il permet de stocker les routes d'api relatives aux comptes qui doivent être identifier par token

export {routerCompte}