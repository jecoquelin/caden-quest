import express from "express";

const routerPersonnage = express.Router()

import {creerJoueur, recupererJoueur, supprimerJoueur, modifierNiveauAptitude, modifierSectionActuelle, recupurerInventaire, ajouterItem, supprimerItem, recupererEnnemi} from '../controllers/personnageControllers.js';

routerPersonnage.post("/creerJoueur", creerJoueur);
routerPersonnage.post("/recupererJoueur", recupererJoueur);
routerPersonnage.delete("/supprimerJoueur", supprimerJoueur);
routerPersonnage.post("/modifierNiveauAptitude", modifierNiveauAptitude);
routerPersonnage.post("/modifierSectionActuelle", modifierSectionActuelle);
routerPersonnage.post("/recupurerInventaire", recupurerInventaire);
routerPersonnage.post("/ajouterItem", ajouterItem);
routerPersonnage.delete("/supprimerItem", supprimerItem);
routerPersonnage.post("/recupererEnnemi", recupererEnnemi);

export {routerPersonnage}