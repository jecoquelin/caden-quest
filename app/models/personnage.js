import { DB } from "../db.js";

export class Personnage {
    
    // Fonction permmettant de récupérer toutes les informations d'un joueur
    static recupererJoueur(idCompte, callback) {
        DB.connect((err, connection) => {
            if (err) {
                connection.release();
                callback(err);
                return ;
            } else {
                connection.query(`
                            SELECT j.id_compte, j.id_personnage, j.id_section_actuelle, p.nom, p.niveau_habilete, p.niveau_endurance, j.niveau_chance, j.niveau_perception
                            FROM cayden_quest._joueur AS j INNER JOIN cayden_quest._personnage AS p ON j.id_personnage = p.id 
                            WHERE j.id_compte = ${idCompte} 
                            LIMIT 1
                        `, (error, result) => {
                    if (error) {
                        connection.release();
                        callback(new Error("Erreur lors de la récupération des informations du joueur : ", error));
                        return ;
                    } else {
                        connection.release();
                        if (result.rowCount == 0) {
                            callback(new Error("Pas de joueur attribué pour ce compte"));
                            return ;
                        }
                        else {
                            callback(null, result.rows[0]);
                            return ;
                        }
                    }
                })
            }
        });
    }

    // Fonction permettant de créer un joueur et au passage un personnage
    static creerJoueur(idCompte, nom, niveauHabilete, niveauEndurance, niveauChance, niveauPerception, callback) {
        // Connexion à la BDD
        DB.connect((err, connection) => {
            // Gestion erreur de connexion
            if (err) {
                callback(err) ;
                return ;
            }

            // Vérification qu'un joueur n'est pas déjà créer pour ce compte
            DB.query(`SELECT * FROM cayden_quest._joueur WHERE id_compte='${idCompte}'`, (error, result) => {
                if (error){
                    connection.release();
                    callback(error);
                    return;
                } else if (result.rows.length == 0) {
                    // Création du personnage 
                    DB.query(`INSERT INTO cayden_quest._personnage (niveau_habilete, niveau_endurance, nom) VALUES (${niveauHabilete}, ${niveauEndurance}, '${nom}') RETURNING id`, (error, result) => {
                        if (error) {
                            connection.release();
                            callback(error);
                            return;
                        }
                        // Création du joueur
                        const idPersonnage = result.rows[0].id;
                        DB.query(`INSERT INTO cayden_quest._joueur (id_compte, id_personnage, id_section_actuelle, niveau_chance, niveau_perception) VALUES (${idCompte}, ${idPersonnage}, 1, ${niveauChance}, ${niveauPerception})`, (error) => {
                            if (error) {
                                connection.release();
                                callback(error);
                                return ;
                            }

                            // Récupération des informations du joueur à jour
                            this.recupererJoueur(idCompte, callback)
                        })
                    })
                } else {
                    connection.release();
                    callback(new Error("Ce compte à déjà son joueur de créer"))
                    return;
                }
            })
        })
    }

    // Fonction permettant de supprimer un joueur à partir de l'identifiant du compte
    static supprimerJoueur(idCompte, callback) {
        DB.connect( (err, connection) => {
            if (err) {
                connection.release()
                callback(err)
            } else {
                // Récupéraiton de l'identifiant du personnage à partir de l'identifiant du compte
                connection.query(`SELECT id_personnage FROM cayden_quest._joueur WHERE id_compte=${idCompte}`, (err, result) => {
                    if (err) {
                        connection.release()
                        callback(new Error("Erreur lors de la récupération de l'identifiant du personnage pour la suppression de ce dernier"))
                        return;
                    } else {
                        connection.query(`DELETE FROM cayden_quest._inventaire WHERE id_compte = ${idCompte}`, (err) => {
                            if (err) {
                                connection.release();
                                callback(new Error("Erreur lors de la suppresion de l'inventaire du joueur"));
                                return ;
                            } else {
                                // Suppression des informations présent dans la table _joueur en premier (FOREIGN KEY)
                                connection.query(`DELETE FROM cayden_quest._joueur WHERE id_personnage=${result.rows[0].id_personnage}`, (err) => {
                                    if (err) {
                                        connection.release()
                                        callback(err)
                                        return ;
                                    }
                                })
                                // Suppression des informations présent dans la table _personnage
                                connection.query(`DELETE FROM cayden_quest._personnage WHERE id=${result.rows[0].id_personnage}`, (err) => {
                                    connection.release()
                                    if (err) {
                                        connection.release()
                                        callback(err)
                                        return ;
                                    }
                                })
                                callback(null, "Joueur supprimé avec succès !")
                                return ;
                            }
                        })
                    }
                })
            }

            
        })
    }

    // Fonction permettant d'augmenter / diminuer le niveau d'une aptitude du joueur
    // nombre de point : la quantité de point à retirer ou ajouter, par défaut 1
    // signe = true, on ajoute || signe = false, on diminue, par défaut true
    static modifierNiveauAptitude(idCompte, aptitude, nombreDePoint=1, signe=true, callback){
        DB.connect( (err, connection) => {
            if (err) {
                connection.release()
                callback(err)
                return ;
            } else {
                // Définition de la table et du nom de la colonne correspondant à l'identifiant du personnage
                let table, idColonne;
                if (aptitude === "habilete" || aptitude === "endurance") {
                    table = "_personnage";
                    idColonne = "id";
                } else if (aptitude === "chance" || aptitude === "perception") {
                    table = "_joueur";
                    idColonne = "id_personnage";
                } else {
                    connection.release()
                    callback(new Error("Aptitude inconnu"));
                    return ;
                }

                // Récupération de l'identifiant du personnage à partir de l'identifiant du compte
                connection.query(`SELECT id_personnage FROM cayden_quest._joueur WHERE id_compte=${idCompte} LIMIT 1`, (err, result) => {
                    if (err) {
                        connection.release()
                        callback(err)
                        return ;
                    } else {
                        let idPersonnage = result.rows[0].id_personnage

                        // Récupération du niveau de l'aptitude à modifier
                        connection.query(`SELECT niveau_${aptitude} as niveau FROM cayden_quest.${table} WHERE ${idColonne}=${idPersonnage} LIMIT 1`, (err, result) => {
                            if (err) {
                                connection.release()
                                callback(err)
                                return ;
                            }
                            else if (result.rows.length == 0) {
                                connection.release()
                                callback(new Error(`Pas de joueur attribué avec l'identifiant ${idPersonnage}`));
                                return ;
                            } else {
                                // Ajout du signe pour ajouter ou enlever n nombre de point
                                let niveau = result.rows[0].niveau + (signe ? nombreDePoint : -nombreDePoint);
                                if (niveau < 0) {
                                    niveau = 0
                                }
                                // Mis à jour du niveau de l'aptitude dans la table concernée
                                connection.query(`UPDATE cayden_quest.${table} SET niveau_${aptitude} = ${niveau} WHERE ${idColonne}=${idPersonnage}`, (err)=> {
                                    connection.release()
                                    if (err) {
                                        connection.release()
                                        callback(new Error("Erreur lors de la mise à jour de l'aptitude " + aptitude + " : ", err))
                                        return ;
                                    } else {
                                        // Récupération des informations du joueur à jour
                                        this.recupererJoueur(idCompte, callback)
                                    }
                                });
                            }
                        })
                    }
                })
            }
        })
    }

    // Fonction permettant de mettre à jour l'identifiant de section du joueur
    static modifierSectionActuelle(idCompte, idSection, callback){
        DB.connect((err, connection) => {
            if (err) {
                connection.release();
                callback(err);
                return ;
            } else {
                connection.query(`UPDATE cayden_quest._joueur SET id_section_actuelle=${idSection} WHERE id_compte=${idCompte}`, (err, result) => {
                    connection.release();
                    if (err) {
                        callback(err)
                        return ;
                    } else {
                        callback(null, "Section du joueur modifié avec succès !")
                        return ;
                    }
                })
            }
        })
    }

    // Fonction permmettant de récupérer les items de l'inventaire
    static recupurerInventaire(idCompte, callback) {
        DB.connect((err, connection) => {
            if (err) {
                connection.release();
                callback(err);
                return ;
            } else {
                connection.query(`SELECT inventaire.id, inventaire.id_compte, inventaire.id_item, item.nom, item.type_effet, item.niveau_effet FROM cayden_quest._inventaire AS inventaire INNER JOIN cayden_quest._item AS item ON inventaire.id_item = item.id WHERE inventaire.id_compte=${idCompte}`, (err, result) => {
                    connection.release();
                    if (err) {
                        callback(err)
                        return ;
                    } else {
                        callback(null, result.rows)
                        return ;
                    }
                })
            }
        })
    }

    // Fonction permettant d'ajouter un item au joueur via son identifiant de compte et retourne l'inventaire à jour
    static ajouterItem(idCompte, idItem, callback) {
        DB.connect( (err, connection) => {
            if (err){
                connection.release();
                callback(err);
                return ;
            } else {
                connection.query(`INSERT INTO cayden_quest._inventaire(id_compte, id_item) VALUES (${idCompte}, ${idItem})`, (err, result) => {
                    connection.release();
                    if (err) {
                        callback(err)
                        return ;
                    } else {
                        this.recupurerInventaire(idCompte, callback)
                        return ;
                    }
                })
            }
        })
    }

    // Fonctio permettant de supprimer un item du joueur via son identifiant de compte
    static supprimerItem(idInventaire, callback) {
        DB.connect( (err, connection) => {
            if (err){
                connection.release();
                callback(err);
                return ;
            } else {
                connection.query(`DELETE FROM cayden_quest._inventaire WHERE id=${idInventaire}`, (err, result) => {
                    connection.release();
                    if (err) {
                        callback(err)
                        return ;
                    } else {
                        callback(null, "Item supprimer de l'inventaire avec succès")
                        return ;
                    }
                })
            }
        })
    };

    // Permet de récupérer un ennemi à partir de son identidiant de section
    static recupererEnnemi(idSection, callback) {
        DB.connect( (err, connection) => {
            if (err){
                connection.release();
                callback(err);
                return ;
            } else {
                connection.query(`SELECT * FROM cayden_quest._ennemi AS e INNER JOIN cayden_quest._personnage AS p ON e.id_personnage = p.id WHERE e.id_section=${idSection}`, (err, result) => {
                    connection.release();
                    if (err) {
                        callback(err)
                        return ;
                    } else {
                        if (result.rows.length === 0) {
                            callback("La section " + idSection + " ne possède pas d'ennemi")
                            return ;
                        } else {
                            callback(null, result.rows)
                            return ;
                        }
                    }
                })
            }
        })
    }
}