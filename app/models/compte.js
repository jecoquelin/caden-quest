import { DB } from "../db.js";
import bcryptjs from 'bcryptjs'

export class Compte {
    static creerCompte(mail, mdp, callback) {
        DB.connect((err, client) => {
            if (err) {
                callback(err)
            }
            DB.query(`SELECT * FROM cayden_quest._compte where email = '${mail}'`, (error, result) => {
                if (error) {
                    client.release();
                    callback(error)
                }
                if (result.rows.length == 0) {
                    let requeteAjoutCompte = `INSERT INTO cayden_quest._compte (email, mot_de_passe) VALUES ('${mail}', '${mdp}')`;
                    DB.query(requeteAjoutCompte, (err, resultInsert) => {
                        if (err) {
                            client.release();
                            callback(err)
                        } else {
                            DB.query(`SELECT * FROM cayden_quest._compte where email = '${mail}'`, (error, resultIdInsert) => {
                                client.release();
                                if (error) {
                                    callback(error)
                                } else {
                                    callback(undefined, resultIdInsert.rows[0].id)
                                }
                            })
                        }
                    })
                } else {
                    client.release();
                    callback(1)
                }
            })
        })
    }

    static connexion(mail, mdp, callback) {
        DB.connect((err, client) => {
            if (err) {
                callback(err)
            }
            DB.query(`SELECT * FROM cayden_quest._compte where email = '${mail}'`, (error, result) => {
                client.release();
                if (error) {
                    callback(error)
                }
                if (result.rows.length == 0) {
                    callback(undefined, 0)
                    return
                }
                bcryptjs.compare(mdp, result.rows[0].mot_de_passe, (err, success) => {
                    if (err) {
                        callback(err)
                    }
                    callback(undefined, success, result.rows[0].id)
                })
            })
        })
    }
}