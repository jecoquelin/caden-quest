import { Compte } from "../models/compte.js";
import bcryptjs from 'bcryptjs'
import jwt from "jsonwebtoken"

function genToken(idCompte, callback) {
    jwt.sign({id: idCompte}, process.env.TOKEN, { expiresIn: "48h" }, (err, token) => {
        callback(undefined, token)
    })
}

function inscription(_req, _res) {
    let mail = _req.body.mail
    bcryptjs.hash(_req.body.mdp, 10, (err, hash) => {
        let mdpHashe = hash
        Compte.creerCompte(mail, mdpHashe, (err, idCompte) => {
            if (err) {
                if (err == 1) {
                    return _res.status(409).send("Email déjà présent")
                } else {
                    return _res.status(500).send("" + err)
                }
            } else {
                genToken(idCompte, (err, token) => {
                    return _res.json({token, idCompte})
                })
            }
        })
    })
}

function connexion(_req, _res) {
    let mail = _req.body.mail
    let mdp = _req.body.mdp
    Compte.connexion(mail, mdp, (err, success, idCompte) => {
        if (err) {
            return _res.status(500).send("Erreur : " + err)
        } else {
            if (success) {
                genToken(idCompte, (err, token) => {
                    return _res.json({token, idCompte})
                })
            } else {
                return _res.status(401).json("Mauvais email ou mot de passe.")
            }
        }
    })
}

export {
    inscription,
    connexion
}