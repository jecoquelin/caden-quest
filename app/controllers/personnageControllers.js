import { Personnage } from "../models/personnage.js";

export function creerJoueur(_req, _res) {
    let idCompte = _req.body.id
    let nom = _req.body.nom
    let niveauHabilete = _req.body.niveauHabilete
    let niveauEndurance = _req.body.niveauEndurance
    let niveauChance = _req.body.niveauChance
    let niveauPerception = _req.body.niveauPerception
    
    Personnage.creerJoueur(idCompte, nom, niveauHabilete, niveauEndurance, niveauChance, niveauPerception, (err, joueur) => {
        if (err) return _res.status(400).send(err)
        else return _res.json(joueur)
    })
}

export function supprimerJoueur(_req, _res) {
    let idCompte = _req.body.id

    Personnage.supprimerJoueur(idCompte, (err, messageSuccess) => {
        if (err) return _res.status(400).send(err)
        else return _res.json({"message": messageSuccess})
    })
}

export function recupererJoueur(_req, _res) {
    let idCompte = _req.body.idCompte

    Personnage.recupererJoueur(idCompte, (err, joueur) => {
        if (err) return _res.status(400).send(err)
        else return _res.json(joueur)
    })
}

export function modifierNiveauAptitude(_req, _res) {
    let idCompte = _req.body.id
    let aptitude = _req.body.aptitude // habilete, endurance, chance, perception
    let nombreDePoint = _req.body.nombreDePoint // 0, 1, 2, 3, ..., n
    let signe = _req.body.signe // true : + | false : -

    Personnage.modifierNiveauAptitude(idCompte, aptitude, nombreDePoint, signe, (err, joueur) => {
        if (err) return _res.status(400).send(err)
        else return _res.json(joueur)
    })
}

export function modifierSectionActuelle(_req, _res) {
    let idCompte = _req.body.idCompte;
    let idSection = _req.body.idSection;
    Personnage.modifierSectionActuelle(idCompte, idSection, (err, messageSuccess)=>{
        if (err) return _res.status(400).send(err)
        else return _res.json({"message": messageSuccess})
    })
}

export function recupurerInventaire(_req, _res) {
    let idCompte = _req.body.idCompte;
    Personnage.recupurerInventaire(idCompte, (error, inventaire) => {
        if (error) {
            return _res.status(400).send(error)
        } else {
            return _res.json(inventaire)
        }
    })
}

export function ajouterItem(_req, _res) {
    let idCompte = _req.body.idCompte;
    let idItem = _req.body.idItem ;
    Personnage.ajouterItem(idCompte, idItem, (error, inventaire) => {
        if (error) {
            return _res.status(400).send(error)
        } else {
            return _res.json(inventaire)
        }
    })
}

export function supprimerItem(_req, _res) {
    let idInventaire = _req.body.idInventaire;
    Personnage.supprimerItem(idInventaire, (error, messageSuccess) => {
        if (error) {
            return _res.status(400).send(error)
        } else {
            return _res.json({"message": messageSuccess})
        }
    })
}

export function recupererEnnemi(_req, _res) {
    let idSection = _req.body.idSection;
    Personnage.recupererEnnemi(idSection, (error, ennemis) => {
        if (error) {
            return _res.status(400).send(error)
        } else {
            return _res.json(ennemis)
        }
    })
}